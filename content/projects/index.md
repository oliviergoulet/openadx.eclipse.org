---
title: "Projects"
date: 2019-04-16T16:09:45+02:00
layout: "single"
headline: "Projects"
hide_sidebar: true
hide_page_title: true
---

{{< eclipsefdn_projects
    templateId="tpl-projects-item"
    url="https://projects.eclipse.org/api/projects?working_group=openadx"
    classes="margin-top-30"
    display_categories="true"
    categories="/data/featured-projects-categories.json"
>}}
